from django.db import models
from django.contrib.auth import get_user_model


class Post(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Title')
    body = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Text')
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_DEFAULT,
        default=1,
        null=False,
        related_name='posts',
        verbose_name='Author'
        )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self):
        return f"#{self.pk} - {self.title}"


class Comment(models.Model):
    text = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Comment')
    author = models.ForeignKey(
        get_user_model(),
        null=True,
        blank=True,
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='comments',
        verbose_name='Author'
        )
    post = models.ForeignKey('blog.Post', on_delete=models.CASCADE, related_name='comments',
                             verbose_name='Post')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creation date and time')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Update date and time')

    def __str__(self) -> str:
        return self.text[:20]
# Create your models here.
