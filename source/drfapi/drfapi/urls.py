"""
URL configuration for drfapi project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from blog.views import PostViewSet, CommentViewSet, UserViewSet, PostCommentsViewSet

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/drf-auth', include('rest_framework.urls')),
    path('api/posts/', PostViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('api/posts/<int:pk>', PostViewSet.as_view({'put': 'update', 'delete': 'destroy', 'get': 'retrieve'})),
    path('api/comments/', CommentViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('api/comments/<int:pk>', CommentViewSet.as_view({'put': 'update', 'delete': 'destroy', 'get': 'retrieve'})),
    path('api/users/', UserViewSet.as_view({'get': 'list', })),
    path('api/posts/<int:post_id>/comments/', PostCommentsViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('api/posts/<int:post_id>/comments/<int:pk>', PostCommentsViewSet.as_view({'put': 'update', 'delete': 'destroy', 'get': 'retrieve'})),

]
